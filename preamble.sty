\ProvidesPackage{preamble}

%% Document class options
\KOMAoptions{
    paper = a4,
    twoside = true, % maybe set to oneside for title page
    fontsize = 12pt,
    chapterprefix = true,
    bibliography = totoc,
    listof = totoc,
    BCOR = 8mm,
    DIV = 14, % Standard for 12pt is DIV = 12
    headsepline = true,
    headings = small,
    headings = optiontohead,
    numbers = noenddot,
    listof = entryprefix, % print Figure/Table prefix in lof/lot
    draft = false, % Prints black bars at the end of overfull hboxes
    headsepline = 0.5pt, % width of the headsepline
}

%% Package to maintain different document versions
%\usepackage{optional}

%% Font configuration
\usepackage{amsfonts,amsmath,amssymb}
\usepackage[no-math]{fontspec} %lualatex%
\usepackage{unicode-math} %lualatex%
\usepackage{microtype}

%% Text font
\setmainfont{texgyretermes-regular.otf}
[
    ItalicFont     = texgyretermes-italic.otf,
    BoldFont       = texgyretermes-bold.otf,
    BoldItalicFont = texgyretermes-bolditalic.otf,
]

%% Sans font
\setsansfont{texgyreheros-regular.otf}
[
    Scale          = MatchUppercase,
    ItalicFont     = texgyreheros-italic.otf,
    BoldFont       = texgyreheros-bold.otf,
    BoldItalicFont = texgyreheros-bolditalic.otf,
]

%% Mono font
\setmonofont{texgyrecursor-regular.otf}
[
    ItalicFont     = texgyrecursor-italic.otf,
    BoldFont       = texgyrecursor-bold.otf,
    BoldItalicFont = texgyrecursor-bolditalic.otf,
]

%% Math font
\setmathfont{texgyretermes-math.otf}

%% Layout and language
\usepackage{geometry} % used for titlepage to temporarily change margins / remove the BCOR
%\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm,includefoot]{geometry} % overrides KOMAoptions (DIV+BCOR) if enabled
\usepackage[onehalfspacing]{setspace}
\recalctypearea % recalculate typearea based on new spacing (otherwise, the heading throws an overfull vbox error on every page)
% \usepackage[main=english,ngerman]{babel} %latex%
\usepackage{polyglossia}%lualatex%
\setdefaultlanguage{english}%lualatex%
\setotherlanguage{german}%lualatex%
\usepackage{makecell}
\usepackage{environ}
\usepackage{ltablex} % combination of tabularx and longtable
\keepXColumns % X columns in tabularx retain automatic scaling behavior
\usepackage{array} % for extended column definitions
%\usepackage{enumerate} % change enumerate layout
\usepackage{circledsteps} % for numbers in circles :-)
\counterwithout{footnote}{chapter} % continuous footnote numbering

%% Paragraph layout
\flushbottom % should be standard for scrbook anyways
\setlength{\parindent}{1em} % 1em is default setting
\setlength{\parskip}{4pt plus 6pt minus 1pt} % 0pt plus 1pt is default setting
%\widowpenalty10000
%\clubpenalty10000

%% Physics specifics
\usepackage{siunitx}[=v2] % bugfix "=v2" for new v3 of siunitx (v3 does not work with the other packages)
\sisetup{ % all optimized for papers -> copy sisetup to indivudual papers before making breaking changes
    locale = US,
    exponent-product = \ensuremath{{}\cdot{}},
    detect-all = true,
    per-mode = symbol,
    inter-unit-product = \ensuremath{{}\cdot{}},
}
\DeclareSIUnit\cal{cal}
\DeclareSIUnit[number-unit-product = {}, group-separator = {,}]\mag{x}
\usepackage{physics}
% \usepackage{miller}
% \usepackage[version=4]{mhchem} % select version because later changes may break compatibility

%% Bibliography
\usepackage[autostyle]{csquotes}
\usepackage[
    hyperref = true,
    backend = biber,
    backref = true, % show backref pages
    backrefstyle = three, % compress backref page ranges with more than 2 pages
    % style = bibliography/custom_style,
    % articletitle = true, % print article titles
    doi = true, % print DOI
    isbn = true, % print ISBN
    maxbibnames = 99, % print all authors
    urldate = long, % spelled-out dates for URL access dates
    dateabbrev = true, % abbreviate the month
    abbreviate = true, % abbreviate bibliographical terms, eg. in the backrefs
]{biblatex}

%% Graphics
\usepackage{graphicx}
\usepackage{caption}
\captionsetup{
    labelfont = bf,
    font = footnotesize,
    format = plain,
    indention = 1em,
    labelsep = colon,
}
% Add new float type for Movies (including List of Movies)
\DeclareCaptionType[fileext = lom, placement = {!ht}]{movie}[Movie][List of Movies]
\DeclareCaptionListFormat{movie}{Movie #1 #2} % set Movie prefix in lom
\captionsetup[movie]{
    listformat = movie, % custom list format for lom (cannot redefine \p@movie because this prefix would also be printed in via \ref)
}

%% PDF specifics
\usepackage{xcolor}
\definecolor{LinkColor}{rgb}{0,0,0.3}
\usepackage{xurl}
\usepackage{hyperxmp}
\usepackage{hyperref}
\hypersetup{
    unicode = true,
    pdftitle = {Title of the Thesis},
    pdfauthor = {Author Name},
    pdfsubject = {Dissertation},
    pdfkeywords = {physics},
    pdfpagelayout = TwoColumnRight,
    breaklinks = true,
    colorlinks = true,
    allcolors = LinkColor,
}
\urlstyle{same} % do not use mono font for URLs
\usepackage{bookmark}
\bookmarksetup{
    numbered = true,
    open = true,
}

%% Design changes
% Chapter heads
% see https://komascript.de/chapterwithlines
\renewcommand{\chapterlineswithprefixformat}[3]{%
  \setlength{\parskip}{0pt}
  {\raggedleft #2}\nobreak
  \rule[.25\baselineskip]{\textwidth}{0.75pt}\\*
  #3\par\nobreak
  \rule[-.2\baselineskip]{\textwidth}{0.75pt}%
}
% Width of table hlines
\setlength{\arrayrulewidth}{0.5pt}

%% Allow pagebreaks in multiline equations
\allowdisplaybreaks

%% Short commands
\newcommand{\hbaromega}{\texorpdfstring{$\hbar \omega$}{hω}} % if used in a section title "hw" is used as a replacement in the bookmarks
